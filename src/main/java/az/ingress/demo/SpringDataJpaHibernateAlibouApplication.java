package az.ingress.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@EnableJpaRepositories(basePackages = {"az.ingress.demo"})
@EntityScan(basePackages = {"az.ingress.demo.entity"})
public class SpringDataJpaHibernateAlibouApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaHibernateAlibouApplication.class, args);
    }

}
