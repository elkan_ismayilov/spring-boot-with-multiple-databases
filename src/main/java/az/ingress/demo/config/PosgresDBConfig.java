package az.ingress.demo.config;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static az.ingress.demo.constant.AppConstant.DDL_AUTO;
import static az.ingress.demo.constant.AppConstant.HIBERNATE_DIALECT_POSTGRE;

/**
 * spring-data-jpa-hibernate-alibou
 * Elkhan
 * 19.02.2024 23:44
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "az.ingress.demo",
        entityManagerFactoryRef = "entityManagerFactoryBeanPostgre",
        transactionManagerRef = "transactionManagerBeanPostgre"
)
@AllArgsConstructor
public class PosgresDBConfig {
    private final Environment env;

    @Bean(name = "dbPostgre")
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("appPostgre.datasource.driverClassName")));
        dataSource.setUrl(env.getProperty("appPostgre.datasource.url"));
        dataSource.setUsername(env.getProperty("appPostgre.datasource.username"));
        dataSource.setPassword(env.getProperty("appPostgre.datasource.password"));
        return dataSource;
    }

    @Bean(name = "entityManagerFactoryBeanPostgre")
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        JpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.dialect", HIBERNATE_DIALECT_POSTGRE);
        properties.put("hibernate.hbm2ddl.auto", DDL_AUTO);
        properties.put("hibernate.show_sql", true);
        properties.put("hibernate.format_sql", true);
        em.setDataSource(dataSource());
        em.setPackagesToScan("az.ingress.demo.entity");
        em.setJpaVendorAdapter(adapter);
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean(name = "transactionManagerBeanPostgre")
    @Primary
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }
}
