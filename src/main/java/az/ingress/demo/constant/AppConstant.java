package az.ingress.demo.constant;

import java.util.Date;

/**
 * spring-data-jpa-hibernate-alibou
 * Elkhan
 * 21.02.2024 23:11
 */
public interface AppConstant {
    Object DDL_AUTO = "create-drop";
    String HIBERNATE_DIALECT_MYSQL = "org.hibernate.dialect.MySQL8Dialect";
    String HIBERNATE_DIALECT_POSTGRE = "org.hibernate.dialect.PostgreSQLDialect";

    default int calculateDaysBetweenDates(Date startDate, Date endDate) {
        return (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
    }
}
