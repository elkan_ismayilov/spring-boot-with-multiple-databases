package az.ingress.demo.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * spring-data-jpa-hibernate-alibou
 * Elkhan
 * 19.02.2024 20:16
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "author")
public class Author {

    @Id
    @GeneratedValue(generator = "author_id_gen", strategy = GenerationType.TABLE)
//    @SequenceGenerator(name = "increment", sequenceName = "author_id_seq", allocationSize = 1)
    @TableGenerator(name = "author_id_gen", table = "hibernate_sequence", pkColumnName = "sequence_name",
            valueColumnName = "sequence_next_hi_value", allocationSize = 1)
    private Integer id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    private String email;
    private Integer age;

}
